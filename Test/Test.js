'use strict';

var expect = require('chai').expect;
var Pokemon = require('../nd5_hw11').Pokemon;
var PokemonList = require('../nd5_hw11').PokemonList;

// Напишите тесты на метод show класса Pokemon.
// Напишите тесты на метод add класса PokemonList.
// Напишите тесты на метод show класса PokemonList.
// Напишите тесты на метод max класса PokemonList.

describe('Класс Pokemon', () => {

    //тесты на метод show класса Pokemon.
    it('в объекте имеется функция show()', () => {
        const poke = new Pokemon('Bulbasaur',1);

        const show = poke["show"];

        expect(typeof show).to.equal("function");
    });

});

describe('Класс PokemonList', () => {

    const list = new PokemonList();
    it('в объекте имеется функция add()', () => {

        const add = list["add"];
        //    list.add('Bulbasaur', 5);

        expect(typeof add).to.equal("function");
    });
    it('метод add() добавляет объекты в массив', () => {

        list.add('Pokesaur', 5);

        expect(list.length).to.equal(1);
    });
    it('в объекте имеется метод show()', () => {

        const show = list["add"];

        expect(typeof show).to.equal("function");
    });

    it('метод max() возвращает покемона с наибольшим уровнем', () => {

        list.add('Bulbasaur', 1);
        const maxPokemon = list.max();

        expect(maxPokemon.level).to.equal(5);
    });


});

