'use strict';

// console.l/og('----- Задание №1 -----');

class Pokemon {
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    show() {
        console.log('Name: %s, level: %d', this.name, this.level);
    }
}

// let poke = new Pokemon('Bulbasaur', 1);
// poke.show();

// console.log('----- Задание №2 -----');

class PokemonList extends Array {
    // constructor(...pokemons) {
    //   super(...pokemons);
    // }
    add(name, level) {
        this.push(new Pokemon(name, level));
    }
}

// console.log('----- Задание №3 -----');
//
// const lost = new PokemonList(new Pokemon('Ivysaur', 2),
//     new Pokemon('Venusaur', 3));
//
// const found = new PokemonList(new Pokemon('Charmander', 4),
//     new Pokemon('Charmeleon', 3));
//
// console.log('Список покемонов в lost:');
// lost.forEach(item => item.show());
//
// console.log('Список покемонов в found:');
// found.forEach(item => item.show());
//
// console.log('----- Задание №4 -----');
// lost.push(new Pokemon('Squirtle', 10));
// found.push(new Pokemon('Weedle', 13));
//
// console.log('----- Задание №5 -----');
//

PokemonList.prototype.show = function () {
    this.forEach(item => {
        item.show();
    });
    console.log('Количество покемонов в списке: ' + this.length);
};

// console.log('Список покемонов в lost:');
// lost.show();
// console.log('Список покемонов в found:');
// found.show();
//
// console.log('----- Задание №6 -----');
// found.push(lost.pop());
//
// console.log('Список покемонов в lost:');
// lost.show();
// console.log('Список покемонов в found:');
// found.show();
//
// console.log('----- Задание №7 -----');

Pokemon.prototype.valueOf = function () {
    return this.level;
};

PokemonList.prototype.max = function () {
    return this.reduce((maxItem, currItem) => (maxItem > currItem) ? maxItem : currItem, this[0]);
};

// console.log('Покемон с максимальным уровнем:', found.max());
//
module.exports.Pokemon = Pokemon;
module.exports.PokemonList = PokemonList;
